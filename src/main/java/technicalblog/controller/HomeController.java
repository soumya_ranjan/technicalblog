package technicalblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import technicalblog.model.Post;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.ui.Model;
import technicalblog.service.PostService;

@Controller
public class HomeController {

    @Autowired    // Dependency injection
    private PostService postService; // Note: Autowired took the work of
                                     // instantiating PostService object using constructor, new PostService()

    // Note: Spring framework initiates new HomeController() and hence we got this print, IOC takes
    // care of garbage collection
    public HomeController(){
        System.out.println("***** HomeController *****");
    }

    @RequestMapping("/")
    public String getAllPosts(Model model) {

        /*PostService service = new PostService();
        ArrayList<Post> allPosts =service.getAllPosts();*/

        List<Post> allPosts = postService.getAllPosts();
        model.addAttribute("all_posts", allPosts);

        return "index";
    }
}
