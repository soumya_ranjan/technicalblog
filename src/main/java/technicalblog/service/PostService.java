package technicalblog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import technicalblog.model.Post;
import technicalblog.repository.PostRepository;

import javax.persistence.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PostService {

    @Autowired
    private PostRepository repository;

    // Note: Spring framework initiates new PostService() and hence we got this print, IOC takes
    // care of garbage collection
    public PostService()
    {
        System.out.println("***** PostService *****");
    }

    public List<Post> getAllPosts()
    {
        return repository.getAllPosts();
    }

    public Post getOnePost()
    {
        return repository.getLatestPost();
    }

    public void createPost(Post newPost)
    {
        // No date field in UI, we want to update the date automatically
        newPost.setDate(new Date());
        repository.createPost(newPost);
        System.out.println("Created Post in post table: " +newPost);
    }

    public Post getPost(Integer postId)
    {
        return repository.getPost(postId);
    }

    public void updatePost( Post updatedPost) {
        updatedPost.setDate(new Date());
        repository.updatePost(updatedPost);
    }

    public void deletePost( Integer postId) {
        repository.deletePost(postId);
    }
}
